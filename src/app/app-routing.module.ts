import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from './auth/auth.guard';


export const routes: Routes = [
  {path: '', loadChildren: 'app/auth/auth.module#AuthModule'},
  {path: 'home', loadChildren: 'app/home/home.module#HomeModule', canActivate: [AuthGuard]},
  {path: 'content', loadChildren: 'app/content/content.module#ContentModule', canActivate: [AuthGuard]},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
