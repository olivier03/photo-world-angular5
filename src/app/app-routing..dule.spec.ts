import { TestBed, async, ComponentFixture, fakeAsync, tick, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthService } from './auth/auth.service';
import { routes } from './app-routing.module';
import { Router } from '@angular/router';
import { AuthModule } from './auth/auth.module';
import { ContentModule } from './content/content.module';
import { HomeModule } from './home/home.module';

import { AuthGuard } from './auth/auth.guard';
import { Inject } from '@angular/core';

class MockAuthGuard {
  constructor(@Inject(Router) private router: Router) {}

  canActivate() {
    this.router.navigate(['/login']);
    return false;
  }
}

describe('AppRoutingModule', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        AuthModule,
        HomeModule,
        ContentModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        NotFoundComponent
      ],
      providers: [{provide: AuthGuard, useClass: MockAuthGuard}, {provide: AuthService}]
    }).compileComponents();
  }));

  it('should not activate route and navigate', async(inject([AuthGuard, Router], (authguard, router) => {
      spyOn(router, 'navigate');
      expect(authguard.canActivate()).toBeFalsy();
      expect(router.navigate).toHaveBeenCalled();
    })
));
  it('should navigate to home', async(inject([Router], (router) => {
    const spy = spyOn(router, 'navigate');
    router.navigate('/home');
    const navArgs = spy.calls.first().args[0];
    expect(navArgs).toBe('/home');
  })
));

});
