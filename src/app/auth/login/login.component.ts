import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AuthService } from '../auth.service';

import * as M from 'materialize-css';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  authState;

  constructor(public authService: AuthService) {

   }

  ngOnInit() {

  }

  login(email, password) {
    this.authService.login(email, password);
  }

}

