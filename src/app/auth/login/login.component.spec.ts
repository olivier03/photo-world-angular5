import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { LoginComponent } from './login.component';

class MockAuthService {
  constructor() {

  }

  login() {

  }

  getAuthState() {
    const authState = {
      uid: 'mockUID'
    };
    return authState;
  }
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ LoginComponent ],
      providers: [{provide: AuthService, useClass: MockAuthService}, {provide: AngularFireAuth}, {provide: firebase}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  afterEach(() => {
    document.body.removeChild(compiled);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render a label for email', async(() => {
    expect(compiled.querySelector('label[for="email"]').textContent).toContain('Email');
  }));
  it('should render a label for password', async(() => {
    expect(compiled.querySelector('label[for="password"]').textContent).toContain('Password');
  }));
  it('should render a button in "a" tag', async(() => {
    expect(compiled.querySelector('a.btn-large').textContent).toContain('Login');
  }));
});
