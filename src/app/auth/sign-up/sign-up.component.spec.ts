import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpComponent } from './sign-up.component';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render a label for email', async(() => {
    expect(compiled.querySelector('label[for="email"]').textContent).toContain('Type your Email');
  }));
  it('should render a label for password', async(() => {
    expect(compiled.querySelector('label[for="password"]').textContent).toContain('Choose a Password');
  }));
  it('should render a button in "a" tag', async(() => {
    expect(compiled.querySelector('a.btn-large').textContent).toContain('Sign Up');
  }));
});
