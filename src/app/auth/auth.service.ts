import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import * as M from 'materialize-css';

@Injectable()
export class AuthService {
  user: String;

  constructor(private firebaseAuth: AngularFireAuth, private router: Router) { }

  login(email: string, password: string) {
    this.firebaseAuth.auth.signInWithEmailAndPassword(email, password).then(success => {
      this.router.navigate(['/home']);
      this.user = success.email;
      M.toast({html: `Welcome back ${this.user}`, classes: 'rounded'});
    }).catch(error => {
      M.toast({html: `Logging in ${error}`, classes: 'rounded'});
    });
  }

  signUp(email: string, password: string) {
    this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password).then(success => {
      M.toast({html: `Signing Up ${success}`, classes: 'rounded'});
    }).catch(error => {
      M.toast({html: `Signing Up ${error}`, classes: 'rounded'});
    });
  }

  getAuthState() {
    return this.firebaseAuth.authState;
  }

  onAuthStateChanged() {
    this.firebaseAuth.auth.onAuthStateChanged(authState => {
      if (authState) {
        M.toast({html: `State changed to ${authState.email}`, classes: 'rounded'});
      }
    }, error => {
      console.log(`Something happened ${error}`);
    });
  }

  logout() {
    this.firebaseAuth.auth.signOut().then(() => {
      M.toast({html: `Sad to see you go ${this.user}`, classes: 'rounded'});
      // this.user = '';
    });
  }

}
