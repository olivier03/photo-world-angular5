import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as M from 'materialize-css';

import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {

   }

  ngOnInit() {
    const options: object = {
      'coverTrigger': false,
      'hover': true
    };
    const elem = document.querySelector('.dropdown-trigger');
    const instance = new M.Dropdown(elem, options);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }

}
