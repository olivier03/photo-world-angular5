import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { HeaderComponent } from './header.component';
import { AuthService } from '../../auth/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      providers: [{provide: AuthService}, {provide: AngularFireAuth}, {provide: firebase}, {provide: Router}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  afterEach(() => {
    document.body.removeChild(compiled);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render a title in a.brand-logo', async(() => {
    expect(compiled.querySelector('a.brand-logo').textContent).toContain('PhotoWorld');
  }));
  it('should render a passage in li a.dropdown-trigger tag', async(() => {
    expect(compiled.querySelector('li a.dropdown-trigger').textContent).toContain('Account');
  }));
});
