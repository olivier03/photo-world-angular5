import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render a passage in h5 tag', async(() => {
    expect(compiled.querySelector('h5').textContent).toContain('PhotoWorld Footer');
  }));
  it('should render a paragraph in p tag', async(() => {
    expect(compiled.querySelector('p#footer-content').textContent).toContain('PhotoWorld');
  }));
  it('should render links in h5 tag', async(() => {
    expect(compiled.querySelector('#links h5').textContent).toContain('Links');
  }));
  it('should render a passage in li tag', async(() => {
    expect(compiled.querySelector('li').textContent).toContain('About');
  }));
  it('should render a copyright message in div tag', async(() => {
    expect(compiled.querySelector('div.footer-copyright div.container').textContent).toContain('© Olivier PhotoWorld 2017 Copyright Text');
  }));
});
