export interface Photo {
    id: string;
    URL: string;
    thumbnailURL: string;
    title: string;
}
