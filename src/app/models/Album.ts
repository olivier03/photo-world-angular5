import { Photo } from './Photo';

export interface Album {
    id: string;
    title: string;
    photos: Photo;
    userID: string;
}
