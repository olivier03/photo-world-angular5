import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Photo } from '../../models/Photo';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

@Injectable()
export class PhotosService {
  photosCollection: AngularFirestoreCollection<Photo>;
  photos: Observable<Photo[]>;

  constructor(db: AngularFirestore) {
    this.photosCollection = db.collection<Photo>('albums').doc('0Zt6vWUp8MuvGqB4BOQl').collection('photos');
    this.photos = this.photosCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Photo;
        data.id = a.payload.doc.id;
        return data;
      });
    });
    console.log('hellooo', this.photos);
   }

   getPhotos() {
     return this.photos;
   }

}
