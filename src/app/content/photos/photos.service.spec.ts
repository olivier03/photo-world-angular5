import { TestBed, inject } from '@angular/core/testing';

import { PhotosService } from './photos.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

class MockPhotosService {}

describe('PhotosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: PhotosService, useClass: MockPhotosService}, {provide: AngularFirestore}, {provide: AngularFirestoreCollection},
        {provide: AngularFirestoreDocument}, {provide: firebase}]
    });
  });

  it('should be created', inject([PhotosService], (service: PhotosService) => {
    expect(service).toBeTruthy();
  }));
});
