import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { PhotosComponent } from './photos.component';
import { PhotosService } from './photos.service';
import { Photo } from '../../models/Photo';
import { Observable } from 'rxjs/Observable';

class MockPhotosService {
  mockPhotos: Observable<Photo[]>;

  constructor() {
    this.mockPhotos = Observable.of([{
      id: 'R1A2N3D4O5M6',
      URL: 'https://i.pinimg.com/736x/b6/d9/61/b6d9613987ecde5444f12c7533bfba51--darth-vader-stencil-star-wars-stencil.jpg',
      thumbnailURL: 'https://i.pinimg.com/736x/de/f4/f1/def4f1d7a435248bf2a64ebc43de468d--darth-vader-face-star-wars-darth.jpg',
      title: 'photoMock1'
    }]);
  }
  getPhotos() {
    return this.mockPhotos;
  }
}

describe('PhotosComponent', () => {
  let component: PhotosComponent;
  let fixture: ComponentFixture<PhotosComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotosComponent ],
      providers: [{provide: PhotosService, useClass: MockPhotosService}]
    })
    .compileComponents();
  }));

  beforeEach(inject([PhotosService], () => {
    fixture = TestBed.createComponent(PhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render the photo title', () => {
    expect(compiled.querySelector('.card-title').textContent).toEqual('photoMock1');
  });
  it('should have one photo', async(() => {
    component.photos.subscribe(photo => {
      expect(photo.length).toEqual(1);
    });
  }));
});
