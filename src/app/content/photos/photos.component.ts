import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PhotosService} from './photos.service';
import {Photo} from '../../models/Photo';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
  photos: Observable<Photo[]>;

  constructor(private photosService: PhotosService) { }

  ngOnInit() {
    this.photos = this.photosService.getPhotos();
  }

}
