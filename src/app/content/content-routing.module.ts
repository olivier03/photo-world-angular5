import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhotosComponent } from './photos/photos.component';
import { AlbumsComponent } from './albums/albums.component';



const routes: Routes = [
    {path: '', component: AlbumsComponent},
    {path: 'albums', component: AlbumsComponent},
    {path: 'photos', component: PhotosComponent},
  //   {path: 'albums',
  //    component: AlbumsComponent,
  //    children: [
  //     {
  //       path: '',
  //       component: AlbumsComponent
  //     },
  //     {
  //       path: ':albumID',
  //       component: PhotosComponent
  //     }
  //   ]
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
