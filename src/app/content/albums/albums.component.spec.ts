import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';

import { AlbumsComponent } from './albums.component';
import { AlbumsService} from './albums.service';
import { Album } from '../../models/Album';

class MockAlbumsService {
  mockAlbums: Observable<Album[]>;

  constructor() {
    this.mockAlbums = Observable.of([{
      id: 'R1A2N3D4O5M6',
      title: 'albumMock1',
      photos: null,
      userID: 'U1S2E3R4',
      }]);
  }

  getAlbums() {
    return this.mockAlbums;
  }
}

describe('AlbumsComponent', () => {
  let component: AlbumsComponent;
  let fixture: ComponentFixture<AlbumsComponent>;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumsComponent ],
      providers: [{provide: AlbumsService, useClass: MockAlbumsService}, {provide: AngularFirestore}]
    })
    .compileComponents();
  }));

  beforeEach(inject([AlbumsService], () => {
    fixture = TestBed.createComponent(AlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  }));

  afterEach(() => {
    document.body.removeChild(compiled);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render the album title', () => {
    expect(compiled.querySelector('.card-title').textContent).toEqual('albumMock1');
  });
  it('should  render the album userID', () => {
    expect(compiled.querySelector('.card-content p').textContent).toContain('U1S2E3R4');
  });
  it('should have one album', async(() => {
    component.albums.subscribe(album => {
      expect(album.length).toEqual(1);
    });
  }));
});
