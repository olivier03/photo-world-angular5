import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AlbumsService} from './albums.service';
import {Album} from '../../models/Album';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {
  albums: Observable<Album[]>;

  constructor(private albumsService: AlbumsService) {

    // this.albumsService.getAlbums().subscribe(albums => {
    //   this.albums = albums;
    //   console.log('In albums', albums);
    // });
  }

  ngOnInit() {

   this.albums = this.albumsService.getAlbums();
  }


}
