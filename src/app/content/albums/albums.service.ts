import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Album } from '../../models/Album';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

@Injectable()
export class AlbumsService {
  albumsCollection: AngularFirestoreCollection<Album>;
  albums: Observable<Album[]>;

  constructor(db: AngularFirestore) {
    this.albumsCollection = db.collection<Album>('albums');
    this.albums = this.albumsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Album;
        data.id = a.payload.doc.id;
        return data;
      });
    });
    console.log('In service', this.albums);
   }

   getAlbums() {
     return this.albums;
   }

}
