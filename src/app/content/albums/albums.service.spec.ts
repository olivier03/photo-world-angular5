import { TestBed, inject } from '@angular/core/testing';

import { AlbumsService } from './albums.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

class MockAlbumsService {}

describe('AlbumsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: AlbumsService, useClass: MockAlbumsService}, {provide: AngularFirestore},
        {provide: AngularFirestoreCollection}, {provide: AngularFirestoreDocument}]
    });
  });

  it('should be created', inject([AlbumsService], (service: AlbumsService) => {
    expect(service).toBeTruthy();
  }));
});
