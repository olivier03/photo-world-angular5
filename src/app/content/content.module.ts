import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotosComponent } from './photos/photos.component';
import { AlbumsComponent } from './albums/albums.component';

import { ContentRoutingModule } from './content-routing.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../../environments/environment';

import { AlbumsService} from './albums/albums.service';
import { PhotosService} from './photos/photos.service';

@NgModule({
  imports: [
    AngularFirestoreModule,
    CommonModule,
    ContentRoutingModule
  ],
  declarations: [
    PhotosComponent,
    AlbumsComponent,
  ],
  providers: [
    AlbumsService,
    PhotosService
  ]
})
export class ContentModule { }
